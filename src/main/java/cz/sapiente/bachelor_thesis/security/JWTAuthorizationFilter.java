package cz.sapiente.bachelor_thesis.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cz.sapiente.bachelor_thesis.entity.UserEntity;
import cz.sapiente.bachelor_thesis.service.AuthService;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer";
    private static final Integer BEARER_LENGTH = 7;

    private final AuthService authService;

    public JWTAuthorizationFilter(AuthenticationManager authManager, AuthService authService) {
        super(authManager);
        this.authService = authService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        final String header = req.getHeader(AUTHORIZATION);

        if (header != null && header.startsWith(BEARER)) {
            final UserEntity user = Optional.ofNullable(this.authService.authenticate(header.substring(BEARER_LENGTH))).orElseThrow(() -> new BadCredentialsException(null));

            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user.getUsername(), null, Collections.emptyList()));
        }

        chain.doFilter(req, res);
    }
}
