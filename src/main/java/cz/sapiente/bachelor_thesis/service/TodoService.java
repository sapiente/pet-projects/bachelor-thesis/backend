package cz.sapiente.bachelor_thesis.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import cz.sapiente.bachelor_thesis.dto.request.CreateTodoRequest;
import cz.sapiente.bachelor_thesis.dto.request.UpdateTodoRequest;
import cz.sapiente.bachelor_thesis.entity.ProjectEntity;
import cz.sapiente.bachelor_thesis.entity.TodoEntity;
import cz.sapiente.bachelor_thesis.entity.UserEntity;
import cz.sapiente.bachelor_thesis.repository.ProjectRepository;
import cz.sapiente.bachelor_thesis.repository.TodoRepository;

@Service
@Transactional
public class TodoService {
    private final TodoRepository todoRepository;
    private final ProjectRepository projectRepository;
    private final AuthService authService;

    public TodoService(final TodoRepository todoRepository, final ProjectRepository projectRepository, final AuthService authService) {
        this.todoRepository = todoRepository;
        this.projectRepository = projectRepository;
        this.authService = authService;
    }

    public List<TodoEntity> findLoggedUserTodos() {
        final UserEntity loggedUser = this.authService.getLoggedUser();

        return this.todoRepository.findTodosByUserId(loggedUser.getId());
    }

    public TodoEntity createTodoForLoggedUser(final CreateTodoRequest request) {
        final UserEntity loggedUser = this.authService.getLoggedUser();
        final ProjectEntity project = Optional.ofNullable(request.getProjectId()).flatMap(this.projectRepository::findById).orElse(null);

        final TodoEntity entity = new TodoEntity();
        entity.setProject(project);
        entity.setUser(loggedUser);
        entity.setName(request.getName());
        entity.setDone(request.getDone());
        entity.setPriority(request.getPriority());
        Optional.ofNullable(request.getDueDate()).map(Date::toInstant).ifPresent(instant -> entity.setDueDate(OffsetDateTime.ofInstant(instant, ZoneId.of("Europe/Prague"))));

        return this.todoRepository.save(entity);
    }

    public void deleteLoggedUserTodo(final long todoId) {
        final TodoEntity entity = this.todoRepository.findById(todoId).orElseThrow(() -> new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY));
        final UserEntity loggedUser = this.authService.getLoggedUser();

        if (! entity.getUser().equals(loggedUser)) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
        }

        this.todoRepository.delete(entity);
    }

    public TodoEntity updateLoggedUserTodo(final long projectId, final UpdateTodoRequest request) {
        final TodoEntity entity = this.todoRepository.findById(projectId).orElseThrow(() -> new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY));
        final UserEntity loggedUser = this.authService.getLoggedUser();

        if (! entity.getUser().equals(loggedUser)) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
        }

        final ProjectEntity project =  Optional.ofNullable(request.getProjectId()).flatMap(this.projectRepository::findById).orElse(null);

        entity.setProject(project);
        entity.setName(request.getName());
        entity.setDone(request.getDone());
        entity.setPriority(request.getPriority());
        Optional.ofNullable(request.getDueDate()).map(Date::toInstant).ifPresent(instant -> entity.setDueDate(OffsetDateTime.ofInstant(instant, ZoneId.of("Europe/Prague"))));

        return this.todoRepository.save(entity);
    }
}
