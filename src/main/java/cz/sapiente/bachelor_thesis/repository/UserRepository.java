package cz.sapiente.bachelor_thesis.repository;

import org.springframework.data.repository.CrudRepository;

import cz.sapiente.bachelor_thesis.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByUsername(String username);
}
