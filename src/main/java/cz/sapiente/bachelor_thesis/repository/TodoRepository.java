package cz.sapiente.bachelor_thesis.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import cz.sapiente.bachelor_thesis.entity.TodoEntity;

public interface TodoRepository extends CrudRepository<TodoEntity, Long> {
    List<TodoEntity> findTodosByUserId(Long id);
    void deleteTodosByProjectId(Long projectId);
}
