package cz.sapiente.bachelor_thesis.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import cz.sapiente.bachelor_thesis.dto.request.CreateTodoRequest;
import cz.sapiente.bachelor_thesis.dto.request.UpdateTodoRequest;
import cz.sapiente.bachelor_thesis.dto.response.TodoResponse;
import cz.sapiente.bachelor_thesis.entity.TodoEntity;
import cz.sapiente.bachelor_thesis.mapper.Mapper;
import cz.sapiente.bachelor_thesis.service.TodoService;

@RestController
@RequestMapping("/todos")
public class TodoRest {

    private final TodoService todoService;
    private final Mapper mapper;

    public TodoRest(final TodoService todoService, final Mapper mapper) {
        this.todoService = todoService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<TodoResponse> all() {
        final List<TodoEntity> entities = this.todoService.findLoggedUserTodos();

        return this.mapper.map(entities, TodoResponse.class);
    }

    @PostMapping
    public TodoResponse create(@RequestBody final CreateTodoRequest request) {
        final TodoEntity entity = this.todoService.createTodoForLoggedUser(request);

        return this.mapper.map(entity, TodoResponse.class);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final long id) {
        this.todoService.deleteLoggedUserTodo(id);
    }

    @PatchMapping("/{id}")
    public TodoResponse update(@PathVariable final long id, @RequestBody final UpdateTodoRequest request) {
        final TodoEntity entity = this.todoService.updateLoggedUserTodo(id, request);

        return this.mapper.map(entity, TodoResponse.class);
    }
}
