package cz.sapiente.bachelor_thesis.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import cz.sapiente.bachelor_thesis.dto.request.LoginRequest;
import cz.sapiente.bachelor_thesis.dto.response.LoginResponse;
import cz.sapiente.bachelor_thesis.service.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthRest {

    private final AuthService service;

    public AuthRest(AuthService service) {
        this.service = service;
    }

    @PostMapping(value = "/login")
    public LoginResponse login(@RequestBody final LoginRequest request) throws UnsupportedEncodingException {
        final String jwt = service.login(request);

        return new LoginResponse(jwt);
    }
}
