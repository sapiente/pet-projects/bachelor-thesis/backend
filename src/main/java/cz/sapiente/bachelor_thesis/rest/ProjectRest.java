package cz.sapiente.bachelor_thesis.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import cz.sapiente.bachelor_thesis.dto.request.CreateProjectRequest;
import cz.sapiente.bachelor_thesis.dto.request.UpdateProjectRequest;
import cz.sapiente.bachelor_thesis.dto.response.ProjectResponse;
import cz.sapiente.bachelor_thesis.entity.ProjectEntity;
import cz.sapiente.bachelor_thesis.mapper.Mapper;
import cz.sapiente.bachelor_thesis.service.ProjectService;

@RestController
@RequestMapping("/projects")
public class ProjectRest {

    private final ProjectService projectService;
    private final Mapper mapper;

    public ProjectRest(final ProjectService projectService, final Mapper mapper) {
        this.projectService = projectService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<ProjectResponse> all() {
        final List<ProjectEntity> entities = this.projectService.findLoggedUserProjects();

        return this.mapper.map(entities, ProjectResponse.class);
    }

    @PostMapping
    public ProjectResponse create(@RequestBody final CreateProjectRequest request) {
        final ProjectEntity entity = this.projectService.createProjectForLoggedUser(request);

        return this.mapper.map(entity, ProjectResponse.class);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final long id) {
        this.projectService.deleteLoggedUserProject(id);
    }

    @PatchMapping("/{id}")
    public ProjectResponse update(@PathVariable final long id, @RequestBody final UpdateProjectRequest request) {
        final ProjectEntity entity = this.projectService.updateLoggedUserProject(id, request);

        return this.mapper.map(entity, ProjectResponse.class);
    }
}
