package cz.sapiente.bachelor_thesis.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import org.springframework.stereotype.Component;

import java.util.List;

import cz.sapiente.bachelor_thesis.dto.request.CreateProjectRequest;
import cz.sapiente.bachelor_thesis.dto.request.UpdateProjectRequest;
import cz.sapiente.bachelor_thesis.entity.ProjectEntity;
import cz.sapiente.bachelor_thesis.service.ProjectService;

@Component
public class ProjectResolver implements GraphQLQueryResolver, GraphQLMutationResolver {

    private final ProjectService projectService;

    public ProjectResolver(final ProjectService projectService) {
        this.projectService = projectService;
    }

    public List<ProjectEntity> getProjects() {
        return this.projectService.findLoggedUserProjects();
    }

    public ProjectEntity createProject(final CreateProjectRequest request) {
        return this.projectService.createProjectForLoggedUser(request);
    }

    public ProjectEntity updateProject(final int id, final UpdateProjectRequest request) {
        return this.projectService.updateLoggedUserProject(id, request);
    }

    public Boolean deleteProject(final int id) {
        this.projectService.deleteLoggedUserProject(id);

        return null;
    }
}
