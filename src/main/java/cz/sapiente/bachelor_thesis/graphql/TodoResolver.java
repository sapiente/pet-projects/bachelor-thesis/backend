package cz.sapiente.bachelor_thesis.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import org.springframework.stereotype.Component;

import java.util.List;

import cz.sapiente.bachelor_thesis.dto.request.CreateTodoRequest;
import cz.sapiente.bachelor_thesis.dto.request.UpdateTodoRequest;
import cz.sapiente.bachelor_thesis.entity.TodoEntity;
import cz.sapiente.bachelor_thesis.service.TodoService;

@Component
public class TodoResolver implements GraphQLQueryResolver, GraphQLMutationResolver {

    private final TodoService todoService;

    public TodoResolver(final TodoService todoService) {
        this.todoService = todoService;
    }

    public List<TodoEntity> getTodos() {
        return this.todoService.findLoggedUserTodos();
    }

    public TodoEntity createTodo(final CreateTodoRequest request) {
        return this.todoService.createTodoForLoggedUser(request);
    }

    public TodoEntity updateTodo(final int id, final UpdateTodoRequest request) {
        return this.todoService.updateLoggedUserTodo(id, request);
    }

    public Boolean deleteTodo(final int id) {
        this.todoService.deleteLoggedUserTodo(id);

        return null;
    }

}
