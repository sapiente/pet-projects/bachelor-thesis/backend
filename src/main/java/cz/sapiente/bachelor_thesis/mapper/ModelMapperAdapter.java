package cz.sapiente.bachelor_thesis.mapper;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ModelMapperAdapter implements Mapper {
    private org.modelmapper.ModelMapper mapper;

    public ModelMapperAdapter() {
        this.mapper = new org.modelmapper.ModelMapper();
    }

    @Override
    public <D> D map(Object source, Class<D> destinationType) {
        return this.mapper.map(source, destinationType);
    }

    @Override
    public <D> List<D> map(final List source, final Class<D> destinationType) {
        return (List<D>) source.stream().map(item -> this.map(item, destinationType)).collect(Collectors.toList());
    }
}
