package cz.sapiente.bachelor_thesis.dto.response;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class TodoResponse {
    private Long id;
    private Long projectId;
    private Long userId;
    private String name;
    private Boolean done;
    private OffsetDateTime dueDate;
    private String priority;
}
