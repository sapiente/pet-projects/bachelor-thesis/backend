package cz.sapiente.bachelor_thesis.dto.response;

import lombok.Data;

@Data
public class ProjectResponse {
    private Long id;
    private Long userId;
    private String name;
}
