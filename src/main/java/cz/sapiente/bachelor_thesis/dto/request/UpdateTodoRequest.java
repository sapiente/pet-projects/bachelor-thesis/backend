package cz.sapiente.bachelor_thesis.dto.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateTodoRequest {
    private Long projectId;
    private String name;
    private Boolean done;
    private Date dueDate;
    private String priority;
}
