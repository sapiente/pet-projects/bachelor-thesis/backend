package cz.sapiente.bachelor_thesis.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "projects")
@NoArgsConstructor
public class ProjectEntity {

    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @Getter
    @Setter
    private String name;

    public ProjectEntity(final UserEntity user, final String name) {
        this.user = user;
        this.name = name;
    }
}
